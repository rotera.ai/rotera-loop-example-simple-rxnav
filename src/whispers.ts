import lang from "~/static/i18n/lang";
import {createSimpleMessageWhisper} from "@rotera.ai/base-ldk-framework/dist/loop/whispers/messageWhispers";
import {DrugConceptGroup, RXNormalDataResponse} from "~/api";
import brandedDrugWhisperTemplate from './templates/brandedDrugWhisperTemplate.hbs';


import {whisper} from '@oliveai/ldk';
import {Loop, WhisperComponent, renderComponents} from '@rotera.ai/base-ldk-framework/dist/utils';
import {Header, Markdown} from '@rotera.ai/base-ldk-framework/dist/components';

/**
 * displaySimpleClipboardWhisper
 *
 * Display the contents of the clipboard event.
 *
 * @param clipboardText The text caught by the clipboard
 */
export function displaySimpleClipboardWhisper(clipboardText: string) {
  createSimpleMessageWhisper({
    label: lang.simpleClipboardWhisper.title,
    content: clipboardText
  });
}

/**
 * displaySimpleDrugResponseWhisper
 *
 * Display the contents of a drug response in a JSON fashion.
 *
 * @param response the RX data response
 */
export function displaySimpleDrugResponseWhisper(response: RXNormalDataResponse) {

  createSimpleMessageWhisper({
    label: lang.simpleDrugResponseWhisper.title,
    content: '```\n' + JSON.stringify(response, null, 2) + '\n```\n'
  });
}

/**
 * displayBrandedDrugResponseWhisper
 *
 * Display the branded drug contents of a drug response in a whisper.
 *
 * @param response the RX data response
 */
export function displayBrandedDrugResponseWhisper(response: RXNormalDataResponse) {

  const sbds = response.drugGroup.conceptGroup.filter((group) => group.tty == 'SBD');

  BrandedDrugResponseWhisper({
    response: response,
    brandedDrugConcepts: sbds
  })
}

/**
 * BrandedDrugResponseWhisperProps
 *
 * The properties for the branded drug response whisper.
 */
type BrandedDrugResponseWhisperProps = {
  /**
   * response
   *
   * The response from the drug API call.
   */
  response: RXNormalDataResponse

  /**
   * brandedDrugConcepts
   *
   * The branded drug concepts from the drug API call.
   */
  brandedDrugConcepts: DrugConceptGroup[]
};

/**
 * BrandedDrugResponseWhisper
 *
 * The whisper showing the branded drug data as a wrapped whisper.
 *
 * @param props the properties for the whisper
 */
export function BrandedDrugResponseWhisper(props: BrandedDrugResponseWhisperProps) {
  Loop.createWhisper({
    props: props,
    methods: {
      getComponents(): WhisperComponent[] {
        const components: WhisperComponent[] = new Array();
        components.push(
          Markdown({
            body: `# ${lang.brandedDrugResponseWhisper.searchHeader(this.response.drugGroup.name)}`
          })
        );

        if (!this.brandedDrugConcepts || this.brandedDrugConcepts.length == 0 ) {
          components.push(
             Markdown({
               body: lang.brandedDrugResponseWhisper.noBrandedDrugsFound
             })
          );
        } else {
          this.brandedDrugConcepts.forEach((conceptGroup) => conceptGroup.conceptProperties.forEach((concept) => {
            components.push(
              Markdown({
                body: `## ${concept.name}`
              }),
              Markdown({
                body: concept.synonym
              })
            )
          }));
        }

        return components;
      },
    },
    lifeCycle: {
      async render() {
        try {
          await whisper.create({
            label: lang.brandedDrugResponseWhisper.title,
            components: renderComponents(...this.getComponents()),
          });
        } catch (e) {
          console.log(e);
        }
      },
    },
  });
}

/**
 * BrandedDrugResponsTemplateeWhisper
 *
 * The whisper showing the branded drug data as a wrapped whisper that uses a rendered
 * Handlebars template.
 *
 * @param props the properties for the whisper
 */
export function BrandedDrugResponseTemplateWhisper(props: BrandedDrugResponseWhisperProps) {
  Loop.createWhisper({
    props: props,
    methods: {
      getComponents(): WhisperComponent[] {
        return [
          Markdown({
            body: brandedDrugWhisperTemplate(props)
          })
        ];
      },
    },
    lifeCycle: {
      async render() {
        try {
          await whisper.create({
            label: lang.brandedDrugResponseWhisper.title,
            components: renderComponents(...this.getComponents()),
          });
        } catch (e) {
          console.log(e);
        }
      },
    },
  });
}
