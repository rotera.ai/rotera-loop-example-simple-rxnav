/**
 * Default example whispers.
 * View each example's source below as well as the package's code to get a
 * better understanding of how the framework works. A TSDoc resource with detailed
 * documentation to come soon!
 *
 * The base-ldk-framework's README also contains extensive details and examples.
 *
 * 1. `$ yarn install`
 * 2. Uncomment code below
 * 3. Open Olive Helps and install a new local loop
 * 4. Run Olive Helps
 * 5. Install this loop.
 * 6. If you make changes,tell the loop to restart.
 */
// import {WhisperSimple} from '@rotera.ai/base-ldk-framework/examples/whisperSimple';
// import {WhisperEvents} from '@rotera.ai/base-ldk-framework/examples/whisperEvents';
// import {WhisperComplex} from '@rotera.ai/base-ldk-framework/examples/whisperComplex';
//
// WhisperSimple();
// WhisperEvents();
// WhisperComplex({mrn: '123'});

import {createSimpleMessageWhisper} from "@rotera.ai/base-ldk-framework/dist/loop/whispers/messageWhispers";
import lang from "~/static/i18n/lang";
import {ui, clipboard} from '@oliveai/ldk';
import {handleClipboardSimpleDrugRequest, handleClipboardSimple, handleClipboardBrandedDrugRequest} from "~/clipboard";

(async () => {
  await displayStartupWhisper();

  ui.loopOpenHandler(async () => await displayStartupWhisper());

  await clipboard.listen(false,
    async (text: string) =>  handleClipboardBrandedDrugRequest(text));
})();

/**
 * displayStartupWhisper()
 *
 * Create a very simple startup whisper.
 */
async function displayStartupWhisper() {
  createSimpleMessageWhisper({
    label: lang.ui.startup.title,
    content: lang.ui.startup.content
  });
}
