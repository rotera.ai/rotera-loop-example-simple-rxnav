/**
 * All of the types and API refer to the National Institutes of Health Drug API for the RXNorm
 * data set.
 *
 * This is found at https://lhncbc.nlm.nih.gov/RxNav/APIs/api-RxNorm.getDrugs.html
 */

import {fetchSimpleAjaxGetRequest} from "@rotera.ai/base-ldk-framework/dist/communication/api/simpleApiClients";

/**
 * The properties of a drug group.
 */
export type DrugConceptProperties = {
  /**
   * The identifier for the drug concept
   */
  rxcui: string

  /**
   * The drug concept name.
   */
  name: string

  /**
   * Short or "Tallman" RxNorm synonym
   */
  synonym: string

  /**
   * The drug term type
   */
  tty: string

  /**
   * LAT attribute from RxNorm
   */
  language: string

  /**
   * The SUPPRESS field from RxNorm
   */
  suppress: string

  /**
   * This field is always blank
   * @param Always
   */
  umlscui: string
}

/**
 * DrugConceptGroup
 *
 * A group of drug concepts.
 */
export type DrugConceptGroup = {
  /**
   *  Term type of the concepts in this group.
   */
  tty: string

  /**
   * All of the drug concept properties for this group.
   */
  conceptProperties: DrugConceptProperties[]
}

/**
 * A drug group from
 */
export type DrugGroup = {

  /**
   * The name of the drug group.
   */
  name: string

  /**
   * The collection of drug concept groups for this druf.
   */
  conceptGroup: DrugConceptGroup[]
}

/**
 * RXNormalDataResponse
 *
 * The response for an RXNorm drug information query.
 */
export type RXNormalDataResponse = {

  /**
   * The drug group data for the drug.
   */
  drugGroup: DrugGroup
}

/**
 * RXNormDrugRequest
 *
 * A request for RXNorm drug information.
 */
export type RXNormDrugRequest = {

  /**
   * The name of the druf
   */
  name: string
}

/**
 * The URL for querying the NIH RXNrom drug database.
 */
const BASE_NIH_RXNORM_DRUG_URL = "https://rxnav.nlm.nih.gov/REST/drugs.json"

/**
 * getRxNormDrugData
 *
 * Get the RXNorm drug data for the given request.
 *
 * @param request The drug request
 */
export async function getRxNormDrugData(request: RXNormDrugRequest): Promise<RXNormalDataResponse> {
  return await fetchSimpleAjaxGetRequest<RXNormDrugRequest, RXNormalDataResponse>(
    BASE_NIH_RXNORM_DRUG_URL,
    request
  )
}