type Dictionary = {
  [id: string]: string | number | Dictionary | {};
};

declare module '*.hbs' {
  const content: (content: Dictionary) => string;
  export default content;
}
