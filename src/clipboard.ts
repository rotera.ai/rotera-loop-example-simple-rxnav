import {createSimpleMessageWhisper} from "@rotera.ai/base-ldk-framework/dist/loop/whispers/messageWhispers";
import {getRxNormDrugData} from "~/api";
import {displayBrandedDrugResponseWhisper, displaySimpleClipboardWhisper, displaySimpleDrugResponseWhisper} from "~/whispers";

/**
 * handleClipboardSimple
 *
 * Handle a clipboard event to give a simple whisper for the event.
 *
 * @param clipboardText The text caught by the clipboard
 */
export async function handleClipboardSimple(clipboardText: string) {
  displaySimpleClipboardWhisper(clipboardText);
}

/**
 * handleClipboardSimpleDrugRequest
 *
 * Handle a clipboard event as a simple drug request.
 *
 * @param clipboardTest The text caught by the clipboard
 */
export async function handleClipboardSimpleDrugRequest(clipboardTest: string) {
  const response = await getRxNormDrugData({
    name: clipboardTest
  });

  displaySimpleDrugResponseWhisper(response);
}

/**
 * handleClipboardBrandedDrugRequest
 *
 * Handle a clipboard event as a simple branded drug request.
 *
 * @param clipboardTest The text caught by the clipboard
 */
export async function handleClipboardBrandedDrugRequest(clipboardTest: string) {
  const response = await getRxNormDrugData({
    name: clipboardTest
  });

  displayBrandedDrugResponseWhisper(response);
}
