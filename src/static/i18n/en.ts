import {displaySimpleDrugResponseWhisper} from "~/whispers";

export default {
  ui: {
    startup: {
      content:
        'This is my first Olive Helps Loop. Is it not it fun!',
      title: 'Hello World!',
    },
  },
  simpleClipboardWhisper: {
    title: 'Hi there from the clipboard'
  },
  simpleDrugResponseWhisper: {
    title: 'The overly computery response'
  },
  brandedDrugResponseWhisper: {
    title: 'Branded Drugs',
    noBrandedDrugsFound: 'No branded drugs found',
    searchHeader: (drugName: string) => `Search results for ${drugName}`
  }
};
