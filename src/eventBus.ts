/***
 * Event Bus
 *
 * The event bus can be used from the `base-ldk-framework`.
 *
 * If needed, additional code can be added here with the combination of the
 * framework's code.
 *
 * @example Emitting and listening to events
 * ```@js
 * import {EventBus} from '@rotera.ai/base-ldk-framework/dist/eventBus';
 *
 * // Global event bus
 * EventBus.emit('event-name', 'argument');
 * EventBus.on('event-name', (param) => {});
 *
 * // Component event bus
 * this.emit('event-name', 'argument');
 * <component>.on('event-name', (param) => {});
 * ```
 *
 * @example Extending EventType
 * // Import framework's `EventType` as an alias
 * import {EventType as BaseEventType} from '@rotera.ai/base-ldk-framework/dist/eventBus';
 *
 * // Create new types for the loop's events
 * enum LoopEventType = { SomeEvent = 'some-event'}
 * // How we can extend `EventType` if the loop needs additionals events besides the ones that
 * // exist in the wrapped components.
 * export const EventType = {...BaseEventType, ...LoopEventType}
 */
