type LoopConfig = {
  name: string;
  version: string;
};

type APIConfig = {
  baseUrl: string;
};

type StaticAssetsConfig = {
  iconsUrl: string;
};

type TelemetryConfig = {
  sentryDsn: string;
};

export type BaseConfig = {
  Loop: LoopConfig;
  API: APIConfig;
  StaticAssets: StaticAssetsConfig;
  Telemetry: TelemetryConfig;
};

declare global {
  const CONFIG: BaseConfig;
}
