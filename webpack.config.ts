/* eslint-disable node/no-unpublished-import */
import ldkConfig from '@oliveai/ldk/dist/webpack/config';
import {merge} from 'webpack-merge';

import * as path from 'path';
import * as dotenv from 'dotenv';
dotenv.config({path: `${__dirname}/.env`});

// eslint-disable-next-line node/no-unpublished-require
const ConfigWebpackPlugin = require('config-webpack');

// Ok for webpack config
// eslint-disable-next-line node/no-unpublished-require
const SpeedMeasurePlugin = require('speed-measure-webpack-plugin');

const smp = new SpeedMeasurePlugin();
module.exports = (env: never, argv: {mode?: string}) => {
  const mode = argv.mode ?? 'production';
  console.log('Building for mode: ' + mode);
  if (!process.env.NODE_CONFIG_ENV) {
    // Force config to follow webpack mode
    process.env.NODE_CONFIG_ENV = mode;
  }

  return smp.wrap(
    merge(ldkConfig, {
      entry: [path.resolve(__dirname, './src/index.ts')],
      plugins: [new ConfigWebpackPlugin()],
      resolve: {
        alias: {
          '~': path.resolve(__dirname, 'src/'),
        },
      },
      module: {
        rules: [
          {
            test: /\.hbs$/,
            loader: 'handlebars-loader',
            options: {
              partialDirs: path.join(__dirname, 'src', 'templates', 'partials'),
            },
          },
          {
            test: /\.m?js$/,
            exclude: /node_modules/,
            loader: 'babel-loader',
            options: {
              cacheCompression: false,
              cacheDirectory: true,
            },
          },
        ],
      },
      optimization: {
        minimize: mode !== 'development',
      },
    })
  );
};
