# The Rotera RXNav Simple Loop Example

This project is an example of a simple loop that uses an API. The loop uses the NIH RXNav drug database to obtain information about various
drugs. The Clipboard aptitude is use to get the drug name.

## To Build

To build the loop, you must first download all the dependencies. This is done with the `yarn` command.

```
yarn install
```

Once this is successful, you can build the loop.

```
yarn build
```

Once the loop compiles successfully, you can install it in Olive Helps as a local loop.


## Commit messages

We use **[semantic-release](https://github.com/semantic-release/semantic-release)** to generate version numbers and releases by parsing our commit messages in the [Angular JS commit message format](https://github.com/angular/angular/blob/master/CONTRIBUTING.md#-commit-message-format).

Here is an example of the release type that will be done based on a commit messages:

| Commit message                                                                                                                                                                                   | Release type               |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | -------------------------- |
| `fix(pencil): stop graphite breaking when too much pressure applied`                                                                                                                             | Patch Release              |
| `feat(pencil): add 'graphiteWidth' option`                                                                                                                                                       | ~~Minor~~ Feature Release  |
| `perf(pencil): remove graphiteWidth option`<br><br>`BREAKING CHANGE: The graphiteWidth option has been removed.`<br>`The default graphite width of 10mm is always used for performance reasons.` | ~~Major~~ Breaking Release |

See the [semantic-release](https://github.com/semantic-release/semantic-release) docs for more information.

## Tooling
- `yarn` Our package manager of choice. Has a very similar command structure to npm. See [yarn getting started guide](https://yarnpkg.com/getting-started).
- `tsc` Transpiles TS files according to your tsconfig.
- `eslint` ensures our team has consistent code and avoids bugs.
- `prettier` ensures our team is coding with a consistent style.

## Prettier configuration

### Jetbrains
- [Prettier vs Linter](https://prettier.io/docs/en/comparison.html)
- [Jetbrains installation](https://prettier.io/docs/en/webstorm.html#using-prettier-with-eslint)
- [Jetbrains configuration](https://plugins.jetbrains.com/plugin/10456-prettier)

## Gotchas
- Network domains must be `https`
- If you update the loop's permissions in `package.json` after you've installed your loop, you'll need to manually update `Olive Helps/secureloops/[loop]/metadata.json` and restart Olive Helps.
- A symlink to `loop.js` must be created in order for OH to auto-update the loop. This step was completed in the **Building a local loop** section while running `yarn symlink`
- Every time `loop.js` changes you must click `Refresh` in the **Loop Library**

## Best practices & things to consider
- Avoid global modules, install them as devDependencies. This will avoid env confusion and issues running the loop command.
- If you need to run a dev dependency with npm you can create a npm script in package.json which will automatically find the binary via the npm_modules alias e.g., `nodemon` is the same as `./node_modules/.bin/nodemon`
- If you need to run a dev dependency with npm directly with the CL, outside of package.json you should be able to use npx.
- Create configuration files for each tool(e.g., .eslint, tsconfig.json) to make code easier to read and to avoid long, inline npm scripts.
- Use ES modules and the absolute alias `~` for imports (e.g., `import {module} ~/helpers/api`)
